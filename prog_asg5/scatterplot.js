/* global d3 */

//Define Color
var colors = d3.scaleOrdinal(d3.schemeCategory10);

//Define Margin
var margin = {left: 80, right: 80, top: 50, bottom: 50 }, 
    width = 960 - margin.left -margin.right,
    height = 500 - margin.top - margin.bottom;

//Define SVG
var svg = d3.select("body")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//Define Scales   
var xScale = d3.scaleLinear()
    .range([0, width]);

var yScale = d3.scaleLinear()
    .range([height, 0]);

// Define Axes
var xAxis = d3.axisBottom(xScale).tickPadding(2);
var yAxis = d3.axisLeft(yScale).tickPadding(2);

// Function for csv row conversion
function rowConverter(d) {
    return {
        country: d.country,
        gdp: +d.gdp,
        population: +d.population,
        epc: +d.ecc,
        total: +d.ec,
        radius: Math.sqrt(+d.total)
    };
}


//**************************************************************************
//    Tooltip Stuff
//**************************************************************************

// tooltip variable
var tooltip = d3.select("body").append("div")	
    .attr("class", "tooltip")				
    .style("opacity", 0);

// handle mouseover event
var mouseOverHandler = function(d){
    tooltip.transition()		
           .duration(200)		
           .style("opacity", .9);
    tooltip.html( d.country + "<br>" +
                  "Population: "+ d.population + "<br>"+
                  "GDP: "+ d.gdp + "<br>"+
                  "ECC: "+ d.epc)
           .style("left", (d3.event.pageX + 10 ) + "px")
           .style("top", (d3.event.pageY - 28) + "px");
}

// handle mouseout event
var mouseOutHandler = function(){
    tooltip.transition()		
           .duration(500)		
           .style("opacity", 0);
}


//**************************************************************************
//    Zoom Stuff
//**************************************************************************

// Zoom Function
var zoom = d3.zoom()
    .on("zoom", zoomHandler);

// zoom listener
var zoomable = svg.append("g").call(zoom);

// handles zooming+panning on each "zoom" event
function zoomHandler(){
  // create new scale ojects based on event
  var new_xScale = d3.event.transform.rescaleX(xScale)
  var new_yScale = d3.event.transform.rescaleY(yScale)

  // update axes with new scales
  gX.call(xAxis.scale(new_xScale));
  gY.call(yAxis.scale(new_yScale));

  // transform circles
  dots.attr("transform", d3.event.transform)
  countryNames.attr("transform", d3.event.transform)
    
<<<<<<< HEAD
xScale.domain([ 0, d3.max(data, function(d) { return d.gdp; }) ]);
    
yScale.domain([
        0,
        d3.max(data, function(d){
            var x = d.epc ;
            return Math.round((50+x)/50)*50;
        })
]);

//Draw Scatterplot
svg.selectAll(".dot")
    .data(data)
    .enter().append("circle")
    .attr("class", "dot")
    .attr("r", function(d) { return Math.sqrt(d.total)/.3; })
    .attr("cx", function(d) {return xScale(d.gdp);})
    .attr("cy", function(d) {return yScale(d.epc);})
    .style("fill", function (d) { return colors(d.country); })
    .on("mouseover", function(){
                        console.log("hello")
                                }
    );

    //Add .on("mouseover", .....
//Add Tooltip.html with transition and style
//Then Add .on("mouseout", ....

//Scale Changes as we Zoom
// Call the function d3.behavior.zoom to Add zoom

//Draw Country Names
svg.selectAll(".text")
    .data(data)
    .enter().append("text")
    .attr("class","text")
    .style("text-anchor", "start")
    .attr("x", function(d) {return xScale(d.gdp);})
    .attr("y", function(d) {return yScale(d.epc);})
    .style("fill", "black")
    .text(function (d) {return d.name; });

//x-axis
svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .append("text")
    .attr("class", "label")
    .attr("y", 50)
    .attr("x", width/2)
    .style("text-anchor", "middle")
    .attr("font-size", "12px")
    .text("GDP (in Trillion US Dollars) in 2010");
=======
}
>>>>>>> 17cc5fbe6b7ad0e8a11c30bdbb74a8f614e7e6ad


//**************************************************************************
//    Declaration of Zoomable Stuff
//**************************************************************************

var dots, countryNames;

// x-axis
var gX = svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")");

// y-axis
var gY = svg.append("g")
        .attr("class", "y axis");


//**************************************************************************
//    Get Data + Draw Scatterplot/Axes/
//**************************************************************************

d3.csv("scatterdata.csv", rowConverter, function(error, data) {
    if (error) throw error;

    // set x and y scale domains based on csv data
    xScale.domain([0, d3.max(data, function(d){return d.gdp;}) ]);

    yScale.domain([0, d3.max(data, function(d){var x = d.epc; return Math.round((50+x)/50)*50;}) ]);

    // Draw scatterplot
    dots = svg.selectAll("dot")
        .data(data)
        .enter().append("circle")
        .attr("class", "dot")
        .attr("r", function(d) { return Math.sqrt(d.total)/.3; })
        .attr("cx", function(d) {return xScale(d.gdp);})
        .attr("cy", function(d) {return yScale(d.epc);})
        .style("fill", function (d) { return colors(d.country); })
        .on("mouseover", mouseOverHandler)
        .on("mouseout", mouseOutHandler);

    // Draw country names
    countryNames = zoomable.selectAll(".text")
        .data(data)
        .enter().append("text")
        .attr("class","text")
        .style("text-anchor", "start")
        .attr("x", function(d) {return xScale(d.gdp)+Math.sqrt(d.total)/.3;})
        .attr("y", function(d) {return yScale(d.epc);})
        .style("fill", "black")
        .text(function (d) {return d.country; });
    
    // Draw x axis
    gY.call(yAxis)
        .append("text")
        .attr("class", "label")
        .attr("transform", "rotate(-90)")
        .attr("y", -50)
        .attr("x", -50)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .style("stroke", "black")
        .attr("font-size", "12px")
        .text("Energy Consumption per Capita (in Million BTUs per person)");
    
    // Draw y axis
    gX.call(xAxis)
        .append("text")
        .attr("class", "label")
        .attr("y", 45)
        .attr("x", width/2)
        .style("text-anchor", "middle")
        .attr("font-size", "12px")
        .style("stroke", "black")
        .text("GDP in 2010 (in Trillion US Dollars)");
})

// draw legend 
svg.append("rect")
    .attr("x", width-150)
    .attr("y", height-190)
    .attr("width", 220)
    .attr("height", 180)
    .attr("fill", "lightgrey")
    .style("stroke-size", "1px");

svg.append("circle")
    .attr("r", 5)
    .attr("cx", width+10)
    .attr("cy", height-175)
    .style("fill", "white");

svg.append("circle")
    .attr("r", 15.8)
    .attr("cx", width+10)
    .attr("cy", height-150)
    .style("fill", "white");

svg.append("circle")
    .attr("r", 50)
    .attr("cx", width+10)
    .attr("cy", height-80)
    .style("fill", "white");

svg.append("text")
    .attr("class", "label")
    .attr("x", width -50)
    .attr("y", height-172)
    .style("text-anchor", "end")
    .text(" 1 Trillion BTUs");

svg.append("text")
    .attr("class", "label")
    .attr("x", width -50)
    .attr("y", height-147)
    .style("text-anchor", "end")
    .text(" 10 Trillion BTUs");

svg.append("text")
    .attr("class", "label")
    .attr("x", width -50)
    .attr("y", height-77)
    .style("text-anchor", "end")
    .text(" 100 Trillion BTUs");

 svg.append("text")
    .attr("class", "label")
    .attr("x", width -50)
    .attr("y", height-15)
    .style("text-anchor", "middle")
    .style("fill", "Green") 
    .attr("font-size", "16px")
    .text("Total Energy Consumption");