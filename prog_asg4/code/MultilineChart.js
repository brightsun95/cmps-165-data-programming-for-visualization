/*global d3*/

var svg = d3.select("svg"),
    margin = {top: 20, right: 80, bottom: 40, left: 50},
    width = svg.attr("width") - margin.left - margin.right,
    height = svg.attr("height") - margin.top - margin.bottom,
    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var xScale = d3.scaleTime().range([0, width]),
    yScale = d3.scaleLinear().range([height,0]),
    zScale = d3.scaleOrdinal(d3.schemeCategory10);

var xAxis = d3.axisBottom(xScale).tickFormat(d3.format("d")),
    yAxis = d3.axisLeft(yScale).ticks(5);


var gridXScale = d3.scaleTime().range([0, width]),
    gridYScale = d3.scaleLinear().range([height,0]);


var gridXAxis = d3.axisTop().scale(gridXScale).ticks(11).tickSize(height, 0, 0).tickFormat(""),
    gridYAxis = d3.axisRight().scale(gridYScale).ticks(5).tickSize(width, 0, 0).tickFormat("");

var t = d3.transition()
            .duration(3000)
            .ease(d3.easeLinear);

var BTU_line = d3.line(t)
    .curve(d3.curveBasis)
    .x(function(d) { return xScale(d.Year); })
    .y(function(d) { return yScale(d.btu); });


d3.csv("BRICSdata.csv", rowConverter, function(error, data) {
    if (error) throw error; 
    var countries = data.columns.slice(1).map(function(id) {
        return {
            id: id,
            values: data.map(function(d) {
                return {Year: d.Year, btu:d[id]}
            })
        };
    });


    xScale.domain(d3.extent(data, function(d) {return d.Year; }));

    yScale.domain([
        d3.min(countries, function(c) {
            return d3.min(c.values, function(d) { return d.btu; });}),
        d3.max(countries, function(c) {
            var x = d3.max(c.values, function(d) { return d.btu })
            return Math.round((50+x)/50)*50; })
    ]);

    zScale.domain(countries.map(function(c) { return c.id; }));

    gridXScale.domain(d3.extent(data, function(d) { return d.Year; }));

    gridYScale.domain([
        d3.min(countries, function(c) {
            return d3.min(c.values, function(d) { return d.btu; });}),
        d3.max(countries, function(c) {
            var x = d3.max(c.values, function(d) { return d.btu })
            return Math.round((50+x)/50)*50; })
    ]);


    // draw vertical gridlines
    g.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .call(gridXAxis);

    // draw horizontal gridlines
    g.append("g")
        .attr("class", "grid")
        .call(gridYAxis);

    g.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
     .append("text")
        .attr("text-anchor", "start")
        .attr("y", 0)
        .attr("dy", 32)
        .attr("x", width/2)
        .attr("fill", "#000")
        .text("Years")
        .attr("font", "12px sans-serif");


    g.append("g")
        .attr("class", "axis axis--y")
        .attr("stroke-width", 1)
        .call(yAxis)
     .append("text")
        .attr("text-anchor", "middle")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "-4em")
        .attr("x", 0)
        .attr("dx", -height/2)
        .attr("fill", "#000")
        .text("Million BTUs Per Person")
        .attr("font", "12px sans-serif");

    var country = g.selectAll("country")
        .data(countries)
        .enter().append("g")
        .attr("class", "country");


    country.append("path")
        .attr("class", "BTU_line")
        .attr("d", function(d) {return BTU_line(d.values); })
        .attr("fill", "none")
        .style("stroke", function(d) { return zScale(d.id); })
        .style("stroke-width", "2px")
        .style("stroke-linecap", "round");

    var totalLength = country.select("path").node().getTotalLength();
    console.log(width);

    country
        .attr("stroke-dasharray", totalLength + " " + totalLength)
        .attr("stroke-dashoffset", totalLength)
        .transition(t)
        .attr("stroke-dashoffset", 0)

    country.append("text")
        .datum(function(d) {
            return {id: d.id, value: d.values[d.values.length-1]}; })
        .attr("transform", function(d) { return "translate(" + xScale(d.value.Year) + "," + yScale(d.value.btu) + ")"; })
        .attr("x", 3)
        .attr("dy", "0.35em")
        .style("font", "12px sans-serif")
        .text(function(d) {return d.id; });

});

function rowConverter(d, _, columns) {
    d.Year = d.Year;
    // dynamically appending object fields
    for (var i = 1, n = columns.length, c; i < n; ++i) d[c = columns[i]] = +d[c];
    return d;
}


